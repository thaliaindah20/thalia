package main

import "fmt"

type listnama struct {
	nama string
}

func main() {
	var nama = []*listnama{{"Ricky"}, {"Pieter"}, {"Palembangan"}, {"Inas"}, {"Salma"}, {"Alifatus"}, {"Thalia"}, {"Indah"}, {"Milagrosa"}, {"Alberto"}}

	printFriends := func([]*listnama) {
		for _, v := range nama {
			fmt.Println(v.nama)
		}
	}

	printFriends(nama)

}
