package main

import (
	"fmt"
)

type Listnama interface {
	Register(u *User) string
	GetUser() []*User
}

type User struct {
	nama string
}

type getNama struct {
	db []*User
}

func main() {
	var db []*User
	userSvc := NewUserService(db)
	resName1 := userSvc.Register(&User{nama: "Ricky"})
	resName2 := userSvc.Register(&User{nama: "Thalia"})
	resName3 := userSvc.Register(&User{nama: "Inas"})
	resName4 := userSvc.Register(&User{nama: "Fisa"})
	resGet := userSvc.GetUser()
	fmt.Println(resName1)
	fmt.Println(resName2)
	fmt.Println(resName3)
	fmt.Println(resName4)
	fmt.Println("----------------Hasil get user-----------------")
	for _, v := range resGet {
		fmt.Println(v.nama)
	}
}

func NewUserService(db []*User) Listnama {
	return &getNama{
		db: db,
	}
}

func (u *getNama) Register(user *User) string {
	u.db = append(u.db, user)
	return user.nama + " berhasil didaftarkan"
}

func (u *getNama) GetUser() []*User {
	return u.db
}
