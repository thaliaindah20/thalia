package main

import (
	"fmt"
	"time"
)

func main() {
	c1 := make(chan int, 2)

	go func(c chan int) {
		for i := 1; i <= 5; i++ {
			fmt.Println("Pertama", i)
			c <- i
			fmt.Println("Kedua", i)
		}
		close(c)
	}(c1)

	fmt.Println("Ketiga")
	time.Sleep(time.Second * 2)

	for v := range c1 { // v = <- c1
		fmt.Println("Keempat", v)
	}

}
