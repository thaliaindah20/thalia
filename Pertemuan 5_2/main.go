package main

import (
	"fmt"
	"time"
)

func main() {
	c1 := make(chan int)

	go func(c chan int) {
		fmt.Println("Pertama")
		c <- 10
		fmt.Println("Kedua")
	}(c1)

	fmt.Println("Ketiga")
	time.Sleep(time.Second * 2)
	fmt.Println("Keempat")
	d := <-c1
	fmt.Println("Kelima", d)

	close(c1)
	time.Sleep(time.Second)
}
