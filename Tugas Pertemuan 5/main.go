package main

import (
	"fmt"
	"math/rand"
)

type status struct {
	player string
	score  int
	win    bool
}

func main() {
	ch := make(chan status)
	players := []string{"PLAYER A", "PLAYER B", "PLAYER C", "PLAYER D"}
	breakPoint := 13
	i := 0

	for {
		randomNumber := rand.Intn(100) + 1
		player := players[i]

		go func(ch chan status, player string, score int) {
			var res status
			res.player = player
			res.score = score

			if score%breakPoint == 0 {
				res.win = false
				ch <- res
				close(ch)
				return
			}

			res.win = true
			ch <- res
			return
		}(ch, player, randomNumber)

		if randomNumber%breakPoint == 0 {
			fmt.Println("break")
			break
		}

		i++
		if i == len(players) {
			i = 0
		}
	}

	hit := 1
	for v := range ch {
		if v.win {
			fmt.Printf("korek ada di player %s pada hit ke %d dan mempunyai nilai %d \n", v.player, hit, v.score)
		} else {
			fmt.Printf("korek ada di player %s pada hit ke %d dan mempunyai nilai %d \n", v.player, hit, v.score)
			fmt.Printf("player %s kalah pada hit ke %d \n", v.player, hit)
			break
		}
		hit++
	}
}
