package main

import "fmt"

func main() {
	for i := 0; i <= 10; i++ {
		if i%2 == 0 {
			fmt.Println(i, "adalah bilangan genap")
		} else if i%2 != 0 {
			fmt.Println(i, "ganjil")
		}
	}
	//Slice
	var nama = []string{"Ricky", "Pieter", "Palembangan", "Inas", "Salma", "Alifatus", "Thalia", "Indah", "Milagrosa", "Alberto"}

	for _, v := range nama {
		fmt.Println(v)
	}
}
