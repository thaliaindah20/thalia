package main

import (
	"fmt"
	"sync"
)

type Listnama interface {
	Register(u *User) string
	GetUser() []*User
}

type User struct {
	nama string
}

type getNama struct {
	db []*User
}

func main() {
	var db []*User
	userSvc := NewUserService(db)
	names := []string{"Ricky", "Inas", "Thalia", "Fisa"}
	var wg1 sync.WaitGroup
	wg1.Add(len(names))

	for _, n := range names {
		go func(name string) {
			res := userSvc.Register(&User{nama: name})
			fmt.Println(res)
			wg1.Done()
		}(n)
	}
	wg1.Wait()

	resGet := userSvc.GetUser()
	fmt.Println("----------------Hasil get user-----------------")
	var wg sync.WaitGroup
	wg.Add(len(resGet))
	for _, v := range resGet {
		go cetakNama(&wg, v.nama)
	}
	wg.Wait()
}

func NewUserService(db []*User) Listnama {
	return &getNama{
		db: db,
	}
}

func (u *getNama) Register(user *User) string {
	u.db = append(u.db, user)
	return user.nama + " berhasil didaftarkan"
}

func (u *getNama) GetUser() []*User {
	return u.db
}

func cetakNama(wg *sync.WaitGroup, nama string) {
	fmt.Println(nama)
	wg.Done()
}
