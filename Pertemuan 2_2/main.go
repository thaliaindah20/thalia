package main

import (
	"fmt"
	"os"
	"strconv"
)

type listnama struct {
	nama      string
	alamat    string
	pekerjaan string
	alasan    string
}

func main() {
	var indexPath = os.Args
	var nama = []*listnama{
		{nama: "Ricky", alamat: "Jl. Aja dulu", pekerjaan: "Backend Developer", alasan: "Ingin menjadi bapak backend Indonesia"},
		{nama: "Pieter", alamat: "Jl.Ku Jalanmu berbeda", pekerjaan: "Backend Developer", alasan: "Ingin menjadi bapak backend Indonesia"},
		{nama: "Palembangan", alamat: "Jl. Bareng Yuk", pekerjaan: "Backend Developer", alasan: "Ingin menjadi bapak backend Indonesia"},
		{nama: "Inas", alamat: "Jl. Ini Indah", pekerjaan: "Backend Developer", alasan: "Ingin menjadi Ibu backend Indonesia"},
		{nama: "Salma", alamat: "Jl. Menuju Surga", pekerjaan: "Backend Developer", alasan: "Ingin menjadi Ibu backend Indonesia"},
		{nama: "Alifatus", alamat: "Jl. Menuju Neraka", pekerjaan: "Backend Developer", alasan: "Ingin menjadi Ibu backend Indonesia"},
		{nama: "Thalia", alamat: "Jl. Tetangga Lebih Subur", pekerjaan: "Frontend Developer", alasan: "Ingin menjadi Fullstack Developer"},
		{nama: "Indah", alamat: "Jl. Kaki ke Pasar", pekerjaan: "Frontend Developer", alasan: "Ingin menjadi Fullstack Developer"},
		{nama: "Milagrosa", alamat: "Jl. Hidup", pekerjaan: "Frontend Developer", alasan: "Ingin menjadi Fullstack Developer"},
		{nama: "Alberto", alamat: "Jl. Kita tak Sama", pekerjaan: "Frontend Developer", alasan: "Ingin menjadi Fullstack Developer"}}

	index, _ := strconv.Atoi(indexPath[1])

	printFriends := func(i int, listnama []*listnama) {
		fmt.Println(listnama[i].nama)
		fmt.Println(listnama[i].alamat)
		fmt.Println(listnama[i].pekerjaan)
		fmt.Println(listnama[i].alasan)
	}

	printFriends(index, nama)

}
