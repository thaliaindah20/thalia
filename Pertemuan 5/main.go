package main

import "fmt"

func main() {
	c := make(chan string)

	go introduce("MNC", c)
	go introduce("Hackaktiv8", c)
	go introduce("A", c)
	go introduce("B", c)

	val := <-c
	val2 := <-c

	fmt.Println(val, val2)

}

func introduce(name string, c chan string) {
	text := fmt.Sprintf("Hello %s", name)

	c <- text
}

//error deadlock --> proses menunggu yang datanya gaada
//case with return for else condition --> needed 
