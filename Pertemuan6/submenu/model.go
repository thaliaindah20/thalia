package submenu

type Employee struct {
	ID       int
	Name     string
	Age      int
	Division string
}

var emps = []Employee{}

func GetEmployee() []Employee {
	return emps
}

func CreateEmployee(emp *Employee) []Employee {
	emps = append(emps, *emp)
	return emps
}
