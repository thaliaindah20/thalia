package submenu

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"text/template"
)

const PORT = ":8080"

func Start() {
	http.HandleFunc("/", hello)
	http.HandleFunc("/employee", getEmployees)
	http.HandleFunc("/create", registerData)

	log.Println("server running at port", PORT)
	http.ListenAndServe(PORT, nil)
}

func hello(w http.ResponseWriter, r *http.Request) {
	msg := "Hello World"
	fmt.Fprintf(w, msg)
}

func getEmployees(w http.ResponseWriter, r *http.Request) {
	employees := GetEmployee()
	tpl, err := template.ParseFiles("index.html")

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	tpl.Execute(w, employees)
	return

	// json.NewEncoder(w).Encode(map[string]interface{}{
	// 	"payload": employees,
	// 	"total":   len(employees),
	// })
}

func registerData(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		var u Employee
		reqBody := json.NewDecoder(r.Body)
		reqBody.Decode(&u)

		u.ID = len(emps) + 1
		employees := CreateEmployee(&u)
		json.NewEncoder(w).Encode(&employees)

	}
}
