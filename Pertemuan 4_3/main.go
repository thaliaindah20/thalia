package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

var PORT = ":8080"
var db []*User

var userSvc = NewUserService(db)

type Listnama interface {
	Register(u *User) string
	GetUser() []*User
}

type User struct {
	Nama string `json:"nama"`
}

type getNama struct {
	db []*User
}

func main() {

	http.HandleFunc("/nama", registerData)
	http.HandleFunc("/get", getData)
	fmt.Println("Application is Listening on Port", PORT)
	http.ListenAndServe(PORT, nil)
}

func NewUserService(db []*User) Listnama {
	return &getNama{
		db: db,
	}
}

func (u *getNama) Register(user *User) string {
	u.db = append(u.db, user)
	return user.Nama + " berhasil didaftarkan"
}

func (u *getNama) GetUser() []*User {
	return u.db
}

func getData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if r.Method == "GET" {
		resGet := userSvc.GetUser()
		json.NewEncoder(w).Encode(resGet)
		//bagian encode .Encode([string]interface{}{
		// "payloads" : resGet
		// })
		return
	}
	http.Error(w, "Invalid Method", http.StatusBadRequest)
}

func registerData(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		var u User
		reqBody := json.NewDecoder(r.Body)
		reqBody.Decode(&u)
		fmt.Println(u.Nama)
		res := userSvc.Register(&u)
		fmt.Println(res)
	}
}
